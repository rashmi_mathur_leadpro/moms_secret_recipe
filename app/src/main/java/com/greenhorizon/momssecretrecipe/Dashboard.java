package com.greenhorizon.momssecretrecipe;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.Toast;

import com.google.android.gms.plus.PlusShare;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.IUserData;
import com.greenhorizon.momssecretrecipe.fragments.NavigationDrawerFragment;
import com.greenhorizon.momssecretrecipe.fragments.PopularRecipesFragment;
import com.greenhorizon.momssecretrecipe.util.StringUtils;


public class Dashboard extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    Fragment fragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragment == null) {
            fragment = new PopularRecipesFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.container, fragment).commit();
        }


    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Intent intent;
        switch(position){
            case 0:
                intent = new Intent(this, ComposeRecipe.class);
                startActivity(intent);
                break;
            case 1:
                intent = new Intent(this, MyRecipeActivity.class);
                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, MyFavoritesActivity.class);
                startActivity(intent);
                break;
            case 3:
            case 4:
                Toast.makeText(getApplicationContext(),
                        "This feature is under Construction", Toast.LENGTH_LONG).show();
                break;

        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.dashboard, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(Dashboard.this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
