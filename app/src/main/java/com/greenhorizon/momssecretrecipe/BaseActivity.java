package com.greenhorizon.momssecretrecipe;

import java.util.List;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.fragments.MomSecretRecipeDialog;
import com.greenhorizon.momssecretrecipe.util.UIHandler;

public class BaseActivity extends ActionBarActivity implements
        MomSecretRecipeDialog.MomSecretDialogCompliant {
	protected AppController controlApplication;
	protected UIHandler uiHandler;
	protected boolean InitializedActivity = false;
	protected Menu optionsMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHandler = new UIHandler("BaseActivity") {

			@Override
			public void onMessage(Message msg) {
				// TODO Auto-generated method stub

			}
		};
		// getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.drawable_title));
		controlApplication = (AppController) getApplication();
		InitializedActivity = true;
	}

	// The internal broadcast receiver for internal actions.
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onReceive(Context context, Intent intent) {
			onReceiveIntent(context, intent);
		}
	};

	/**
	 * Set up the intent listener for this activity with the specified actions.
	 * 
	 * @param intentActions
	 *            The intent actions.
	 */
	protected void setIntentActions(List<String> intentActions) {
		IntentFilter intentFilter = new IntentFilter();
		if (intentActions != null) {
			for (String action : intentActions) {
				intentFilter.addAction(action);
			}
		}
        AppController.getInstance().registerReceiver(
				mBroadcastReceiver, intentFilter);
	}

	/**
	 * Set up the intent listener for this activity with the specified actions.
	 * 
	 * @param
	 *
	 */
	protected void unsetIntentActions() {
        AppController.getInstance().unregisterReceiver(
				mBroadcastReceiver);
	}

	/**
	 * Fires when an intent is received for this activity.
	 * 
	 * @param context
	 *            The context for the intent.
	 * @param intent
	 *            The intent.
	 */
	protected void onReceiveIntent(Context context, Intent intent) {
		System.out.print("gsfgsfgsfgsfgsfgsfgsfgfs");
	}

	protected MomSecretRecipeDialog progressDialog;
	protected MomSecretRecipeDialog dialog;

	// private String errorMessage;

	protected void setPercentage(final int percentage) {
		if (progressDialog != null && progressDialog.getDialog() != null)
			((ProgressDialog) progressDialog.getDialog())
					.setProgress(percentage);
	}

	public void showProgress(final String message, final boolean cancelable) {
		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				progressDialog = new MomSecretRecipeDialog(BaseActivity.this,
						R.string.app_name, message,
                        MomSecretRecipeDialog.PROGRESS_DIALOG, cancelable);
				progressDialog.show(getSupportFragmentManager(), "");
			}
		});
	}

	protected void showProgressPercentage(final String message,
			final boolean cancelable) {
		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				progressDialog = new MomSecretRecipeDialog(BaseActivity.this,
						R.string.app_name, message,
                        MomSecretRecipeDialog.PROGRESS_PERCENTAGE_DIALOG, cancelable);
				progressDialog.show(getSupportFragmentManager(), "");
			}
		});
	}

	protected void showProgress(final String message, final boolean cancelable,
			final OnCancelListener listener) {

		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				try {
					progressDialog = new MomSecretRecipeDialog(BaseActivity.this,
							R.string.app_name, message,
                            MomSecretRecipeDialog.PROGRESS_DIALOG, cancelable);
					progressDialog.show(getSupportFragmentManager(), "");
					// if (cancelable && listener != null)
					// progressDialog.setOnCancelListener(listener);
				} catch (Exception e) {

				}
			}
		});
	}

	protected void cancelProgress() {
		if (progressDialog != null) {
			progressDialog.dismiss();
		}

	}

	protected void cancelProgressPercentage() {
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
	}

	public void showError(final String message, final String tag) {
		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				// errorMessage = message;
				try {
					if (progressDialog != null) {
						cancelProgress();
					}

					dialog = new MomSecretRecipeDialog(BaseActivity.this,
							R.string.app_name, message,
                            MomSecretRecipeDialog.ERROR_MESSAGE_DIALOG, tag);
					dialog.show(getSupportFragmentManager(), "");
					// removeDialog(ERROR_MESSAGE_DIALOG);
				} catch (Exception e) {

				}
				// showDialog(ERROR_MESSAGE_DIALOG);
			}
		});
	}

	public void showInfo(final String message, final String tag) {
		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				try {
					if (progressDialog != null) {
						cancelProgress();
					}
					dialog = new MomSecretRecipeDialog(BaseActivity.this,
							R.string.app_name, message,
                            MomSecretRecipeDialog.ALERT_DIALOG, tag);
					dialog.show(getSupportFragmentManager(), "");
				} catch (Exception e) {

				}
			}
		});
	}

    public void showConfirmationDialog(final String message, final String tag) {
		uiHandler.post(new Runnable() {
			@Override
			public void run() {
				// errorMessage = message;
				try {
					if (progressDialog != null) {
						cancelProgress();
					}

					dialog = new MomSecretRecipeDialog(BaseActivity.this,
							R.string.app_name, message,
                            MomSecretRecipeDialog.CONFIRMATION_DIALOG, tag);
					dialog.show(getSupportFragmentManager(), "");
				} catch (Exception e) {

				}
			}
		});
	}

    public void showRadioDialog(final String message, final String tag) {
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                // errorMessage = message;
                try {
                    if (progressDialog != null) {
                        cancelProgress();
                    }

                    dialog = new MomSecretRecipeDialog(BaseActivity.this,
                            R.string.app_name, message,
                            MomSecretRecipeDialog.RADIOBUTTON_DIALOG, tag);
                    dialog.show(getSupportFragmentManager(), "");
                } catch (Exception e) {

                }
            }
        });
    }
	
	@Override
	protected void onDestroy() {
		UIHandler handler = controlApplication.getUIHandler();
		if (uiHandler != null && handler != null && uiHandler.equals(handler)) {
			controlApplication.setUIHandler(null);
			uiHandler = null;
		}
		super.onDestroy();
	}

	@Override
	public void doOkConfirmClick(String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doCancelConfirmClick(String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doOkClick(String tag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doOkErrorDailogClick(String tag) {
		// TODO Auto-generated method stub

	}

    @Override
    public void onRadioSelection(int status){

    }

    @Override
    public void onRadioSelected(){

    }

	public void selectItem(int position) {

	}

	@Override
	protected void onStop() {
		super.onStop();
	}
}