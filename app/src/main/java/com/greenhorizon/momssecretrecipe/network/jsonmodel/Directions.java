package com.greenhorizon.momssecretrecipe.network.jsonmodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 06/03/15.
 */
public class Directions {

    @SerializedName("direction")
    public String direction;
}
