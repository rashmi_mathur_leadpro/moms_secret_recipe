package com.greenhorizon.momssecretrecipe.network.jsonmodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 07/03/15.
 */

public class PopularRecipeModel {

    @SerializedName("popularRecipes")
    public ArrayList<PopularRecipe> popularRecipes;

    public PopularRecipeModel() {
        popularRecipes = new ArrayList<PopularRecipe>();
    }
}
