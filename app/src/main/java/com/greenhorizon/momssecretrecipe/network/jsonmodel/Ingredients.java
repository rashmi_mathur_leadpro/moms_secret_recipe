package com.greenhorizon.momssecretrecipe.network.jsonmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rashmimathur on 06/03/15.
 */
public class Ingredients {
    @SerializedName("ingredient")
    public String ingredient;
}
