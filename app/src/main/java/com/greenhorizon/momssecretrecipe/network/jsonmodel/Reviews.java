package com.greenhorizon.momssecretrecipe.network.jsonmodel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rashmimathur on 07/03/15.
 */
public class Reviews {

    @SerializedName("reviewername")
    public String reviewername;

    @SerializedName("reviewerpic")
    public String reviewerpic;

    @SerializedName("comment")
    public String comment;

    @SerializedName("reviewedon")
    public String reviewedon;

    @SerializedName("rating")
    public float rating;
}
