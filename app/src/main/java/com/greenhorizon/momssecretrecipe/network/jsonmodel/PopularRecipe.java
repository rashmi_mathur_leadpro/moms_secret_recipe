package com.greenhorizon.momssecretrecipe.network.jsonmodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 06/03/15.
 */
public class PopularRecipe {

    @SerializedName("id")
    public int id;

    @SerializedName("recipename")
    public String recipename;

    @SerializedName("recipeid")
    public String recipeid;

    @SerializedName("rating")
    public float rating;

    @SerializedName("imageUrl")
    public String imageUrl;

    @SerializedName("ingredients")
    public ArrayList<Ingredients> ingredients;

    @SerializedName("directions")
    public ArrayList<Directions> directions;

    @SerializedName("reviews")
    public ArrayList<Reviews> reviews;

    @SerializedName("profilename")
    public String profilename;

    @SerializedName("profilepic")
    public String profilepic;

    public PopularRecipe() {
        ingredients = new ArrayList<Ingredients>();
        directions = new ArrayList<Directions>();
        reviews = new ArrayList<Reviews>();
    }

}
