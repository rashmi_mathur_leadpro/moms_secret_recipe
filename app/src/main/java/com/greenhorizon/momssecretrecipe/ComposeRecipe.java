package com.greenhorizon.momssecretrecipe;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Directions;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Ingredients;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.MyRecipe;
import com.greenhorizon.momssecretrecipe.util.StringUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Vector;


public class ComposeRecipe extends BaseActivity {

    private ViewGroup mContainerView;
    private Button nbtnAddIngredients;
    private EditText edtIngredient;
    private MyRecipe myRecipeEntity;

    private ViewGroup mContainerDirectiomView;
    private Button nbtnAddDirections;
    private EditText edtDirection;
    private ImageView imgCoverPicture;

    private EditText edtTitle;

    private RelativeLayout rlCoverPicture;
    private static int RESULT_LOAD_IMAGE = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private String SDCARD_PATH = "/mnt/sdcard/MomRecipe/";

    private LinkedHashMap<Integer, String> hashMapDirections;
    private LinkedHashMap<Integer, String> hashMapIngredients;
    private Button btnPublishRecipe;
    private int directions = 0;
    private int ingredients = 0;
    private String filePath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_recipe);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        hashMapDirections = new LinkedHashMap<Integer, String>();
        hashMapIngredients = new LinkedHashMap<Integer, String>();

        edtTitle = (EditText) findViewById(R.id.txt_title);
        btnPublishRecipe = (Button) findViewById(R.id.btn_publishrecipe);
        btnPublishRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(StringUtils.IsStringNotEmpty(edtTitle.getText().toString().trim()) && hashMapDirections.size() > 0
                        && hashMapIngredients.size()>0 && StringUtils.IsStringNotEmpty(filePath.trim())){
                    myRecipeEntity = new MyRecipe();

                    myRecipeEntity.recipename = edtTitle.getText().toString().trim();
                    myRecipeEntity.imageUrl = filePath;
                    Iterator<String> itIngredients = hashMapIngredients.values().iterator();
                    while (itIngredients.hasNext())
                    {
                        String ingredient = itIngredients.next();
                        Ingredients mIngredient = new Ingredients();
                        mIngredient.ingredient = ingredient;
                        myRecipeEntity.ingredients.add(mIngredient);
                    }

                    Iterator<String> itDirections = hashMapDirections.values().iterator();
                    while (itDirections.hasNext())
                    {
                        String direction = itDirections.next();
                        Directions mDirection = new Directions();
                        mDirection.direction = direction;
                        myRecipeEntity.directions.add(mDirection);
                    }
                    AppController.getInstance().getDaoService().getMyRecipesDao().
                            addMyRecipe(new MyRecipesEntity(-1, AppController.getInstance().getGsonObj().toJson(myRecipeEntity).toString(),
                                    filePath, 0));
                    Intent intent = new Intent(ComposeRecipe.this, MyRecipeActivity.class);
                    startActivity(intent);
                    finish();

                }else{
                    showError(getResources().getString(R.string.fill_in_all_details), "addrecipe_err");
                }
            }
        });

        rlCoverPicture  = (RelativeLayout) findViewById(R.id.cover_photo);
        imgCoverPicture = (ImageView) findViewById(R.id.coverpicture);
        rlCoverPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddPhoto();
            }
        });
        mContainerView = (ViewGroup) findViewById(R.id.container);

        edtIngredient = (EditText) findViewById(R.id.txt_ingredient);

        nbtnAddIngredients = (Button) findViewById(R.id.btn_addingredients);
        nbtnAddIngredients.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if(StringUtils.IsStringNotEmpty(edtIngredient.getText().toString().trim()))
                    addItem(edtIngredient.getText().toString());
                else
                    showError(getResources().getString(R.string.no_ingredients_err), "error");
            }
        });

        mContainerDirectiomView = (ViewGroup) findViewById(R.id.directionscontainer);

        edtDirection = (EditText) findViewById(R.id.txt_direction);

        nbtnAddDirections = (Button) findViewById(R.id.btn_adddirections);
        nbtnAddDirections.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if(StringUtils.IsStringNotEmpty(edtDirection.getText().toString().trim()))
                    addDirection(edtDirection.getText().toString());
                else
                    showError(getResources().getString(R.string.no_directions_err), "error");

            }
        });
    }

    private void addItem(String ingredient) {
        // Instantiate a new "row" view.
        final ViewGroup newView = (ViewGroup) LayoutInflater.from(this).inflate(
                R.layout.ingredient_list_item, mContainerView, false);

        // Set the text in the new row to a random country.
        ((TextView) newView.findViewById(android.R.id.text1)).setText(ingredient);
        ingredients++;
        hashMapIngredients.put(ingredients, ingredient);
        newView.setTag(ingredients);
        edtIngredient.setText("");

        if(hashMapIngredients.size() > 0){
            findViewById(android.R.id.empty).setVisibility(View.GONE);
        }else{
            findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
        }

        // Set a click listener for the "X" button in the row that will remove the row.
        newView.findViewById(R.id.delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hashMapIngredients.remove((int) newView.getTag());
                // Remove the row from its parent (the container view).
                // Because mContainerView has android:animateLayoutChanges set to true,
                // this removal is automatically animated.
                mContainerView.removeView(newView);

                // If there are no rows remaining, show the empty view.
                if (mContainerView.getChildCount() == 0) {
                    findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
                }
            }
        });

        // Because mContainerView has android:animateLayoutChanges set to true,
        // adding this view is automatically animated.
        mContainerView.addView(newView, 0);
    }

    private void addDirection(String direction) {
        // Instantiate a new "row" view.
        final ViewGroup newView = (ViewGroup) LayoutInflater.from(this).inflate(
                R.layout.direction_list_item, mContainerDirectiomView, false);

        // Set the text in the new row to a random country.
        ((TextView) newView.findViewById(android.R.id.text1)).setText(direction);
        directions++;
        hashMapDirections.put(directions, direction);
        newView.setTag(directions);
        edtDirection.setText("");

        if(hashMapDirections.size() > 0){
            findViewById(R.id.empty_directions_lbl).setVisibility(View.GONE);
        }else{
            findViewById(R.id.empty_directions_lbl).setVisibility(View.VISIBLE);
        }

        // Set a click listener for the "X" button in the row that will remove the row.
        newView.findViewById(R.id.delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hashMapDirections.remove((int)newView.getTag());
                // Remove the row from its parent (the container view).
                // Because mContainerView has android:animateLayoutChanges set to true,
                // this removal is automatically animated.
                mContainerDirectiomView.removeView(newView);

                // If there are no rows remaining, show the empty view.
                if (mContainerDirectiomView.getChildCount() == 0) {
                    findViewById(R.id.empty_directions_lbl).setVisibility(View.VISIBLE);
                }
            }
        });

        // Because mContainerView has android:animateLayoutChanges set to true,
        // adding this view is automatically animated.
        mContainerDirectiomView.addView(newView, 0);
    }

    Uri outputFileUri;
    public static String filename;
    private void openAddPhoto() {

        String[] addPhoto=new String[]{ getResources().getString(R.string.camera) , getResources().getString(R.string.gallery) };
        AlertDialog.Builder dialog=new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.take_pic_lbl));

        dialog.setItems(addPhoto,new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int id) {
                Intent intent;
                if(id==0){
                    // create Intent to take a picture and return control to the calling
                    // application
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File filedir = new File(Environment.getExternalStorageDirectory()
                            + "/MomRecipe");
                    filedir.mkdirs();

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                            .format(new Date());
                    File file = new File(Environment.getExternalStorageDirectory()
                            + "/MomRecipe", "Recipe" + timeStamp + ".jpg");
                    outputFileUri = Uri.fromFile(file);
                    filename = "Recipe" + timeStamp + ".jpg";
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    // start the image capture Intent
                    startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                }
                if(id==1){
                    intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(intent, RESULT_LOAD_IMAGE);
                }
            }
        });

        dialog.setNeutralButton("cancel",new android.content.DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }});
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            imgCoverPicture.setImageBitmap(BitmapFactory.decodeFile(picturePath,
                    options));
            filePath = picturePath;

        } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent
                Toast.makeText(
                        this,
                        getResources().getString(R.string.img_save_lbl),
                        Toast.LENGTH_LONG).show();
                if (StringUtils.IsStringNotEmpty(filename)) {
                    sendBroadcast(new Intent(
                            Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                            Uri.parse(outputFileUri.toString())));
                    loadPictureTaken(filename);
                }
            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture failed, advise user
            }
        }

    }

    private void loadPictureTaken(String name) {
        File file = new File(SDCARD_PATH + filename);
        if (file.exists()) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeFile(SDCARD_PATH + filename,
                    options);

            imgCoverPicture.setImageBitmap(bitmap);

            filePath = SDCARD_PATH + filename;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                intent = new Intent(getApplicationContext(),
                        Dashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
