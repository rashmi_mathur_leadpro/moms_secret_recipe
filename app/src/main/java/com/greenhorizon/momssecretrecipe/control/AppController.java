package com.greenhorizon.momssecretrecipe.control;

/**
 * Created by rashmimathur oon 06/03/15.
 */
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.gson.Gson;
import com.greenhorizon.momssecretrecipe.Dashboard;
import com.greenhorizon.momssecretrecipe.LoginActivity;
import com.greenhorizon.momssecretrecipe.SettingsActivity;
import com.greenhorizon.momssecretrecipe.database.IUserData;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.database.model.UserDataEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Directions;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Ingredients;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.PopularRecipe;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.PopularRecipeModel;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Reviews;
import com.greenhorizon.momssecretrecipe.services.IDaoService;
import com.greenhorizon.momssecretrecipe.services.impl.DaoServiceImpl;
import com.greenhorizon.momssecretrecipe.util.LruBitmapCache;
import com.greenhorizon.momssecretrecipe.util.StringUtils;
import com.greenhorizon.momssecretrecipe.util.UIHandler;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Locale;

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;
    private static final String DAO_SERVICE = "DAO_SERVICE";
    private IDaoService daoService;

    private Gson gsonObj;
    private UIHandler uiHandler;

    private GoogleApiClient googleAPIClient;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        IUserData userData = AppController.getInstance().getDaoService().getUserDataDao();
        String lang = userData.getValue(IUserData.LANGUAGE);
//        if(StringUtils.IsStringEmpty(lang)){
//            lang = "en";
//        }
        setLocale(lang, this);


    }

    public void setLocale(String lang, Context context) {
        if(StringUtils.IsStringEmpty(lang)){
            return;
        }
        Locale myLocale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        loadTestData(lang);


    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public IDaoService getDaoService() {
        synchronized (DAO_SERVICE) {
            if (daoService == null) {
                daoService = new DaoServiceImpl();
            }
        }
        return daoService;
    }

    public Gson getGsonObj() {
        if (gsonObj == null) {
            gsonObj = new Gson();
        }
        return gsonObj;
    }

    public void setGoogleAPIClient(GoogleApiClient client) {
        this.googleAPIClient = client;
    }

    public GoogleApiClient getGoogleAPIClient() {
        return googleAPIClient;
    }

    public void loadTestData(String lang){
        getDaoService().getPopularRecipesDao().deleteAllPopularRecipes();
        ArrayList<PopularRecipesEntity> arrPopularRecipes = getDaoService().getPopularRecipesDao().getPopularRecipes();

//        if(arrPopularRecipes != null && arrPopularRecipes.size() <= 0){
        String data = getRecipeData(lang);
        if(StringUtils.IsStringNotEmpty(data)){
            PopularRecipeModel popularRecipeModel = getGsonObj().fromJson(data, PopularRecipeModel.class);
            ArrayList<PopularRecipe> arrRecipes = popularRecipeModel.popularRecipes;
            if(arrRecipes != null && arrRecipes.size() > 0)
            {

                for(PopularRecipe recipe: arrRecipes) {
                    getDaoService().getPopularRecipesDao().addPopularRecipe(new PopularRecipesEntity(recipe.id, getGsonObj().toJson(recipe).toString(), recipe.imageUrl, 1));
                }
            }
            String firstTime = AppController.getInstance().getDaoService().getUserDataDao().getValue(IUserData.FIRST_TIME);
            if(StringUtils.IsStringEmpty(firstTime)){
                Intent refresh = new Intent(this, LoginActivity.class);
                refresh.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(refresh);
                AppController.getInstance().getDaoService().getUserDataDao().insertKey(new UserDataEntity(IUserData.FIRST_TIME, "false"));
            }
            else {
                Intent refresh = new Intent(this, Dashboard.class);
                refresh.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(refresh);
            }
//            }
        }
    }

    protected String getRecipeData(String lang) {
        String data = "";
        try {
            // Open your local db as the input stream
            InputStream myInput = this.getAssets().open("recipe_data_"+lang+".txt");

            // Open the empty db as the output stream
            ByteArrayOutputStream myOutput = new ByteArrayOutputStream();

            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            data = new String(myOutput.toByteArray());
            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return data;
    }

    private Directions getDirectionsObj(String direction){
        Directions directions = new Directions();
        directions.direction = direction;
        return directions;
    }

    private Ingredients getIngredientsObj(String ingredient){
        Ingredients ingredients = new Ingredients();
        ingredients.ingredient = ingredient;
        return ingredients;
    }

    private Reviews getReviewsObj(String reviewername, String picurl, float rating, String comment, String date){
        Reviews reviews = new Reviews();
        reviews.reviewername = reviewername;
        reviews.reviewerpic = picurl;
        reviews.rating = rating;
        reviews.comment = comment;
        reviews.reviewedon = date;
        return reviews;
    }

    public void setUIHandler(UIHandler uiHandler) {
        this.uiHandler = uiHandler;
    }

    public UIHandler getUIHandler() {
        return uiHandler;
    }
}