package com.greenhorizon.momssecretrecipe.util;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public abstract class UIHandler extends Handler implements Parcelable {

	private String componentName;
    private String TAG = "UIHandler";
	private Activity activity;

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public Activity getActivity() {
		return activity;
	}

	public UIHandler(String componentName) {
		super();
		this.componentName = componentName;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	@Override
	public void handleMessage(Message msg) {
		super.handleMessage(msg);
		try {
			if (msg != null) {
				onMessage(msg);
			} else {
				Log.d(TAG, "Invalid message");
			}
		} catch (Exception e) {
            Log.d(TAG, ""+e);
		}

	}

	public abstract void onMessage(Message msg);

	public int describeContents() {

		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}

}
