package com.greenhorizon.momssecretrecipe.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.greenhorizon.momssecretrecipe.R;
import com.greenhorizon.momssecretrecipe.control.AppController;

import java.io.InputStream;

/**
 * Created by rashmimathur on 07/03/15.
 */
public class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    private static final int PROFILE_PIC_SIZE = 100;
    private static final String TAG = "LoadProfileImage";
    public LoadProfileImage(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }


    /**
     * Fetching user's information name, email, profile pic
     * */
    public static void getProfileInformation(ImageView img) {
        try {
            GoogleApiClient googleAPIClient = AppController.getInstance().getGoogleAPIClient();
            if (googleAPIClient != null  && Plus.PeopleApi.getCurrentPerson(googleAPIClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(googleAPIClient);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(googleAPIClient);

                Log.e(TAG, "Name: " + personName + ", plusProfile: "
                        + personGooglePlusProfile + ", email: " + email
                        + ", Image: " + personPhotoUrl);

                // by default the profile url gives 50x50 px image only
                // we can replace the value with whatever dimension we want by
                // replacing sz=X
                personPhotoUrl = personPhotoUrl.substring(0,
                        personPhotoUrl.length() - 2)
                        + PROFILE_PIC_SIZE;

                new LoadProfileImage(img).execute(personPhotoUrl);

            } else {
//                Toast.makeText(getApplicationContext(),
//                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Fetching user's name
     * */
    public static String getProfileName(Context context) {
        String personName = context.getResources().getString(R.string.guest);
        try {
            GoogleApiClient googleAPIClient = AppController.getInstance().getGoogleAPIClient();
            if (googleAPIClient != null  && Plus.PeopleApi.getCurrentPerson(googleAPIClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(googleAPIClient);
                personName = currentPerson.getDisplayName();
            } else {
//                Toast.makeText(context,
//                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return personName;
    }

    /**
     * Fetching user's information name, email, profile pic
     * */
    public static String getProfilePicUrl(Context context) {
        String profilePicUrl = "";
        try {
            GoogleApiClient googleAPIClient = AppController.getInstance().getGoogleAPIClient();
            if (googleAPIClient != null  && Plus.PeopleApi.getCurrentPerson(googleAPIClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(googleAPIClient);
                profilePicUrl = currentPerson.getImage().getUrl();
            } else {
//                Toast.makeText(context,
//                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return profilePicUrl;
    }
}
