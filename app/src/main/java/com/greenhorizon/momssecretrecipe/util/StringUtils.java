package com.greenhorizon.momssecretrecipe.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.TimeZone;

import android.content.Context;
import android.telephony.TelephonyManager;

public class StringUtils {

	private static final String loggerName = StringUtils
			.getClassName(StringUtils.class);

	public static String getClassName(Class<?> c) {
		String name = c.getName();
		int firstChar;
		firstChar = name.lastIndexOf('.') + 1;
		if (firstChar > 0) {
			name = name.substring(firstChar);
		}
		return name;
	}

	public static int getIntValue(String val) {
		int value = -1;
		try {
			value = Integer.parseInt(val);
		} catch (Exception e) {

		}
		return value;
	}

	public static long getLongValue(String val) {
		long value = -1;
		try {
			value = Long.valueOf(val);
		} catch (Exception e) {

		}
		return value;
	}

	public static boolean IsStringEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static boolean IsStringNotEmpty(String str) {
		return !IsStringEmpty(str);
	}

}
