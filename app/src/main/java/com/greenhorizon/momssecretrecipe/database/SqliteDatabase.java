package com.greenhorizon.momssecretrecipe.database;

import android.database.sqlite.SQLiteDatabase;

public interface SqliteDatabase {

  /**
   * Called when the database is first created.
   */
  void onCreate();

  /**
   * Called when the database is upgraded.
   * 
   * @param oldVersion
   *            The old database version.
   * @param newVersion
   *            The new database version.
   */
  void onUpgrade(int oldVersion, int newVersion);

  SQLiteDatabase getWritableDatabase();
  
  SQLiteDatabase getReadableDatabase();
}
