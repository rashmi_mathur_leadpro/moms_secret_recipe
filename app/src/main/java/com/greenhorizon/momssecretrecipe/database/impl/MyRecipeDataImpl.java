package com.greenhorizon.momssecretrecipe.database.impl;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.IMyRecipesData;
import com.greenhorizon.momssecretrecipe.database.contentprovider.ContentValuesFactory;
import com.greenhorizon.momssecretrecipe.database.contentprovider.DatastoreContract;
import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;

import java.util.ArrayList;

public class MyRecipeDataImpl implements IMyRecipesData {

	private static final String TAG = "MyRecipeDataImpl";

	@Override
	public void addMyRecipe(MyRecipesEntity myRecipesEntity) {
		ContentValues values = ContentValuesFactory
				.contentValuesFor(myRecipesEntity);
		if (myRecipesEntity != null
				&& !keyAlreadyExists(DatastoreContract.MyRecipes.CONTENT_URI,
                DatastoreContract.MyRecipes.COLUMN_RECIPE_ID,
						"" + myRecipesEntity.getmId())) {
            AppController.getInstance()
					.getContentResolver().insert(DatastoreContract.MyRecipes.CONTENT_URI, values);
		} else {
            AppController.getInstance()
					.getContentResolver()
					.update(DatastoreContract.MyRecipes.CONTENT_URI, values,
                            DatastoreContract.MyRecipes.COLUMN_RECIPE_ID + " = ?",
							new String[] { "" + myRecipesEntity.getmId() });
		}

	}

	@Override
	public ArrayList<MyRecipesEntity> getMyRecipes() {
		Cursor cursor = null;
		ArrayList<MyRecipesEntity> entities = new ArrayList<MyRecipesEntity>();
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.MyRecipes.CONTENT_URI, null, null,
							null, null);
			if (cursor != null && cursor.moveToFirst()) {

				do {
					entities.add(loadFromMyRecipeCursor(cursor));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return entities;
	}
	
	@Override
	public MyRecipesEntity getMyRecipe(String columnName,
			String value) {
		Cursor cursor = null;
        MyRecipesEntity entity = null;
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.MyRecipes.CONTENT_URI, null, columnName + " = ?",
							new String[] { value }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				entity = loadFromMyRecipeCursor(cursor);
			}
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return entity;
	}

	public static final MyRecipesEntity loadFromMyRecipeCursor(Cursor c) {
        MyRecipesEntity myRecipesEntity = new MyRecipesEntity(c.getInt(0),
				c.getInt(1), c.getString(2), c.getString(3), c.getInt(4));
		return myRecipesEntity;
	}

	public boolean keyAlreadyExists(Uri uri, String columnname, String key) {
		Cursor cursor = null;
		boolean exists = false;
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(uri, null, columnname + " = ?",
							new String[] { key }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				exists = true;
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return exists;
	}

	public MyRecipesEntity keyAlreadyExists(String columnname, String key) {
		Cursor cursor = null;
        MyRecipesEntity entity = null;
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.MyRecipes.CONTENT_URI, null, columnname + " = ?",
							new String[] { key }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				entity = loadFromMyRecipeCursor(cursor);
			}
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return entity;
	}

	@Override
	public void deleteAllMyRecipes() {
		try {

            AppController.getInstance()
					.getContentResolver()
					.delete(DatastoreContract.MyRecipes.CONTENT_URI, null, null);
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		}
	}

	@Override
	public void deleteMyRecipe(String columnName, String value) {
		try {

            AppController.getInstance()
					.getContentResolver()
					.delete(DatastoreContract.MyRecipes.CONTENT_URI, columnName + " = ?",
							new String[] { value });
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		}

	}


	@Override
	public void markAsFav(String columnName, String value) {
		final ContentValues values = new ContentValues();
		values.put(DatastoreContract.MyRecipes.COLUMN_IS_FAV,
				1);
        AppController.getInstance()
				.getContentResolver()
				.update(DatastoreContract.MyRecipes.CONTENT_URI, values, columnName + " = ?",
						new String[] { "" + value });

	}

    @Override
    public void markAsUnFav(String columnName, String value) {
        final ContentValues values = new ContentValues();
        values.put(DatastoreContract.MyRecipes.COLUMN_IS_FAV,
                0);
        AppController.getInstance()
                .getContentResolver()
                .update(DatastoreContract.MyRecipes.CONTENT_URI, values, columnName + " = ?",
                        new String[] { "" + value });

    }
}
