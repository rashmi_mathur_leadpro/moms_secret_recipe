package com.greenhorizon.momssecretrecipe.database;

import java.util.ArrayList;

import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;

public interface IPopularRecipesData {

	public void addPopularRecipe(PopularRecipesEntity popularRecipesEntity);

	public ArrayList<PopularRecipesEntity> getPopularRecipes();

    public ArrayList<PopularRecipesEntity> getFavRecipes();
	
	public PopularRecipesEntity getPopularRecipe(String columnName,
                                       String value);

	public void deletePopularRecipe(String columnName, String value);

	public void deleteAllPopularRecipes();

	public void markAsFav(String columnName, String value);

    public void markAsUnFav(String columnName, String value);

}
