package com.greenhorizon.momssecretrecipe.database.model;

public class PopularRecipesEntity {
    private int mId;
    private int mrecipeID;
    private String mrecipeData;
    private String mrecipeImgUrl;
    private int isFav;

    public PopularRecipesEntity() {

    }

    public PopularRecipesEntity(int mId, int mrecipeID, String mrecipeData, String mrecipeImgUrl, int isFav) {
        this.mId = mId;
        this.mrecipeID = mrecipeID;
        this.mrecipeData = mrecipeData;
        this.mrecipeImgUrl = mrecipeImgUrl;
        this.isFav = isFav;
    }

    public PopularRecipesEntity(int mrecipeID, String mrecipeData, String mrecipeImgUrl, int isFav) {
        this.mrecipeID = mrecipeID;
        this.mrecipeData = mrecipeData;
        this.mrecipeImgUrl = mrecipeImgUrl;
        this.isFav = isFav;
    }

    public int getMrecipeID() {
        return mrecipeID;
    }

    public void setMrecipeID(int mrecipeID) {
        this.mrecipeID = mrecipeID;
    }

    public String getMrecipeData() {
        return mrecipeData;
    }

    public void setMrecipeDate(String mrecipeData) {
        this.mrecipeData = mrecipeData;
    }

    public String getMrecipeImgUrl() {
        return mrecipeImgUrl;
    }

    public void setMrecipeImgUrl(String mrecipeImgUrl) {
        this.mrecipeImgUrl = mrecipeImgUrl;
    }

    public int getIsFav() {
        return isFav;
    }

    public void setIsFav(int isFav) {
        this.isFav = isFav;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

}
