package com.greenhorizon.momssecretrecipe.database;

import com.greenhorizon.momssecretrecipe.database.model.UserDataEntity;

public interface IUserData {

	public static final String FIRST_NAME = "firstname";
	public static final String EMAIL = "email";
	public static final String LAST_NAME = "lastname";
	public static final String MOBILE_NUMBER = "mobileno";
	public static final String REGISTRATION_STATUS = "registration_status";
    public static final String LANGUAGE = "language";
    public static final String FIRST_TIME = "first_time";
	
	public void insertKey(UserDataEntity userDataEntity);
	
	public String getValue(String key);
	
	public void deleteAllUserDetails();
}
