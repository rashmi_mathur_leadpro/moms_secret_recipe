package com.greenhorizon.momssecretrecipe.database.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import com.greenhorizon.momssecretrecipe.database.contentprovider.DatastoreContract;
import android.net.Uri;

import com.greenhorizon.momssecretrecipe.database.SqliteDatabaseUtils;

public class SecretRecipeContentProvider extends android.content.ContentProvider {

	private SqliteDatabaseUtils helper = null;

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int match = sURIMatcher.match(uri);
		SQLiteDatabase database;
		int rows;
		switch (match) {
		case USERDATA:
			database = helper.getWritableDatabase();
			rows = database.delete(DatastoreContract.UserData.TABLE_NAME, selection,
					selectionArgs);
			return rows;
		case POPULAR_RECIPES:
			database = helper.getWritableDatabase();
			rows = database.delete(DatastoreContract.PopularRecipes.TABLE_NAME, selection,
					selectionArgs);
			return rows;
		case MY_RECIPES:
			database = helper.getWritableDatabase();
			rows = database.delete(DatastoreContract.MyRecipes.TABLE_NAME, selection,
					selectionArgs);
			return rows;
		case UriMatcher.NO_MATCH:
			throw new IllegalArgumentException("Unknown Uri:" + uri);
		default:
			break;
		}

		return -1;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		int match = sURIMatcher.match(uri);
		long id = -1;
		SQLiteDatabase db;
		switch (match) {
		case USERDATA:
			db = helper.getWritableDatabase();
			id = db.insert(DatastoreContract.UserData.TABLE_NAME, null, values);
			if (id != -1) {
				return ContentUris.withAppendedId(DatastoreContract.UserData.CONTENT_URI, id);
			}
			break;
		case POPULAR_RECIPES:
			db = helper.getWritableDatabase();
			id = db.insert(DatastoreContract.PopularRecipes.TABLE_NAME, null, values);
			if (id != -1) {
				return ContentUris.withAppendedId(DatastoreContract.PopularRecipes.CONTENT_URI, id);
			}
			break;
		case MY_RECIPES:
			db = helper.getWritableDatabase();
			id = db.insert(DatastoreContract.MyRecipes.TABLE_NAME, null, values);
			if (id != -1) {
				return ContentUris
						.withAppendedId(DatastoreContract.MyRecipes.CONTENT_URI, id);
			}
			break;
		case UriMatcher.NO_MATCH:
			throw new IllegalArgumentException("Unknown Uri:" + uri);
		}

		return null;
	}

	@Override
	public boolean onCreate() {
		helper = new SqliteDatabaseUtils(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		int match = sURIMatcher.match(uri);
		SQLiteDatabase readableDb = helper.getReadableDatabase();
		SQLiteQueryBuilder qb;
		switch (match) {
		case USERDATA:
			qb = new SQLiteQueryBuilder();
			qb.setTables(DatastoreContract.UserData.TABLE_NAME);
			return qb.query(readableDb, projection, selection, selectionArgs,
					null, null, sortOrder);
		case POPULAR_RECIPES:
			qb = new SQLiteQueryBuilder();
			qb.setTables(DatastoreContract.PopularRecipes.TABLE_NAME);
			return qb.query(readableDb, projection, selection, selectionArgs,
					null, null, sortOrder);
		case MY_RECIPES:
			qb = new SQLiteQueryBuilder();
			qb.setTables(DatastoreContract.MyRecipes.TABLE_NAME);
			return qb.query(readableDb, projection, selection, selectionArgs,
					null, null, sortOrder);
		case UriMatcher.NO_MATCH:
			throw new IllegalArgumentException("Unknown Uri:" + uri);
		default:
			break;
		}

		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int match = sURIMatcher.match(uri);
		int id = -1;
		SQLiteDatabase db;
		switch (match) {
		case USERDATA:
			db = helper.getWritableDatabase();
			String whereClause = DatastoreContract.UserData.KEY + "= ?";
			id = db.update(DatastoreContract.UserData.TABLE_NAME, values, whereClause,
					selectionArgs);
			return id;
		case POPULAR_RECIPES:
			db = helper.getWritableDatabase();
			id = db.update(DatastoreContract.PopularRecipes.TABLE_NAME, values, selection,
					selectionArgs);
			return id;
		case MY_RECIPES:
			db = helper.getWritableDatabase();
			id = db.update(DatastoreContract.MyRecipes.TABLE_NAME, values, selection,
					selectionArgs);
			return id;
		case UriMatcher.NO_MATCH:
			throw new IllegalArgumentException("Unknown Uri:" + uri);

		default:
			break;
		}

		return -1;
	}

	private static final int USERDATA = 1;
	private static final int POPULAR_RECIPES = 2;
	private static final int MY_RECIPES = 3;

	private static final UriMatcher sURIMatcher = new UriMatcher(
			UriMatcher.NO_MATCH);

	static {
		sURIMatcher.addURI(DatastoreContract.AUTHORITY,
                DatastoreContract.PopularRecipes.TABLE_NAME, POPULAR_RECIPES);
		sURIMatcher.addURI(DatastoreContract.AUTHORITY,
                DatastoreContract.MyRecipes.TABLE_NAME, MY_RECIPES);
		sURIMatcher.addURI(DatastoreContract.AUTHORITY,
                DatastoreContract.UserData.TABLE_NAME, USERDATA);
	}

}
