package com.greenhorizon.momssecretrecipe.database.impl;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.IUserData;
import com.greenhorizon.momssecretrecipe.database.contentprovider.ContentValuesFactory;
import com.greenhorizon.momssecretrecipe.database.contentprovider.DatastoreContract;
import com.greenhorizon.momssecretrecipe.database.model.UserDataEntity;
import com.greenhorizon.momssecretrecipe.util.StringUtils;

public class UserDataImpl implements IUserData {

    private static String TAG = "UserDataImpl";

	@Override
	public void insertKey(UserDataEntity userDataEntity) {
		if (userDataEntity != null
				&& StringUtils.IsStringNotEmpty(userDataEntity.getmKey())) {
			if (!keyAlreadyExists(userDataEntity.getmKey())) {
				ContentValues values = ContentValuesFactory
						.contentValuesFor(userDataEntity);
				AppController.getInstance()
						.getContentResolver()
						.insert(DatastoreContract.UserData.CONTENT_URI, values);
			} else {
				ContentValues values = ContentValuesFactory
						.contentValuesFor(userDataEntity);
                AppController.getInstance()
						.getContentResolver()
						.update(DatastoreContract.UserData.CONTENT_URI, values,
								userDataEntity.getmKey(),
								new String[] { userDataEntity.getmKey() });
			}
		}

	}

	private boolean keyAlreadyExists(String key) {
		Cursor cursor = null;
		boolean exists = false;
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.UserData.CONTENT_URI, null, DatastoreContract.UserData.KEY + " = ?",
							new String[] { key }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				exists = true;
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return exists;
	}

	@Override
	public String getValue(String key) {
		Cursor cursor = null;
		String value = "";
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.UserData.CONTENT_URI, null, DatastoreContract.UserData.KEY + " = ?",
							new String[] { key }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				value = cursor.getString(1);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return value;
	}

	@Override
	public void deleteAllUserDetails() {
		try {

            AppController.getInstance()
					.getContentResolver().delete(DatastoreContract.UserData.CONTENT_URI, null, null);
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		}
	}

}
