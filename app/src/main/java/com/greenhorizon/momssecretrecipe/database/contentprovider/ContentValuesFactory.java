package com.greenhorizon.momssecretrecipe.database.contentprovider;

import android.content.ContentValues;

import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.database.model.UserDataEntity;

public class ContentValuesFactory {
    public static ContentValues contentValuesFor(UserDataEntity userData) {
        ContentValues values = new ContentValues();
        values.put(DatastoreContract.UserData.KEY, userData.getmKey());
        values.put(DatastoreContract.UserData.VALUE, userData.getmValue());
        return values;
    }

	public static ContentValues contentValuesFor(PopularRecipesEntity popularRecipeEntity) {
		ContentValues values = new ContentValues();
		if(popularRecipeEntity.getMrecipeID() > 0)
		values.put(DatastoreContract.PopularRecipes.COLUMN_RECIPE_ID, popularRecipeEntity.getMrecipeID());
		values.put(DatastoreContract.PopularRecipes.COLUMN_RECIPE_DATA, popularRecipeEntity.getMrecipeData());
		values.put(DatastoreContract.PopularRecipes.COLUMN_RECIPE_IMG_URL, popularRecipeEntity.getMrecipeImgUrl());
		values.put(DatastoreContract.PopularRecipes.COLUMN_IS_FAV, popularRecipeEntity.getIsFav());
		return values;
	}
	
	public static ContentValues contentValuesFor(MyRecipesEntity myRecipeEntity) {
        ContentValues values = new ContentValues();
        if(myRecipeEntity.getMrecipeID() > 0)
            values.put(DatastoreContract.PopularRecipes.COLUMN_RECIPE_ID, myRecipeEntity.getMrecipeID());
        values.put(DatastoreContract.PopularRecipes.COLUMN_RECIPE_DATA, myRecipeEntity.getMrecipeData());
        values.put(DatastoreContract.PopularRecipes.COLUMN_RECIPE_IMG_URL, myRecipeEntity.getMrecipeImgUrl());
        values.put(DatastoreContract.PopularRecipes.COLUMN_IS_FAV, myRecipeEntity.getIsFav());
        return values;
	}
}
