package com.greenhorizon.momssecretrecipe.database;

import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;

import java.util.ArrayList;

public interface IMyRecipesData {

	public void addMyRecipe(MyRecipesEntity myRecipesEntity);

	public ArrayList<MyRecipesEntity> getMyRecipes();
	
	public MyRecipesEntity getMyRecipe(String columnName,
                                                 String value);

	public void deleteMyRecipe(String columnName, String value);

	public void deleteAllMyRecipes();

	public void markAsFav(String columnName, String value);

    public void markAsUnFav(String columnName, String value);

}
