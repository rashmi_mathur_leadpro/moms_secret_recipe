package com.greenhorizon.momssecretrecipe.database.impl;

import java.util.ArrayList;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.IPopularRecipesData;
import com.greenhorizon.momssecretrecipe.database.contentprovider.ContentValuesFactory;
import com.greenhorizon.momssecretrecipe.database.contentprovider.DatastoreContract;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.util.StringUtils;

public class PopularRecipeDataImpl implements IPopularRecipesData {

	private static final String TAG = "PopularRecipeDataImpl";

	@Override
	public void addPopularRecipe(PopularRecipesEntity popularRecipesEntity) {
		ContentValues values = ContentValuesFactory
				.contentValuesFor(popularRecipesEntity);
		if (popularRecipesEntity != null
				&& !keyAlreadyExists(DatastoreContract.PopularRecipes.CONTENT_URI,
                DatastoreContract.PopularRecipes.COLUMN_RECIPE_ID,
						"" + popularRecipesEntity.getmId())) {
            AppController.getInstance()
					.getContentResolver().insert(DatastoreContract.PopularRecipes.CONTENT_URI, values);
		} else {
            AppController.getInstance()
					.getContentResolver()
					.update(DatastoreContract.PopularRecipes.CONTENT_URI, values,
                            DatastoreContract.PopularRecipes.COLUMN_RECIPE_ID + " = ?",
							new String[] { "" + popularRecipesEntity.getmId() });
		}

	}

	@Override
	public ArrayList<PopularRecipesEntity> getPopularRecipes() {
		Cursor cursor = null;
		ArrayList<PopularRecipesEntity> entities = new ArrayList<PopularRecipesEntity>();
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.PopularRecipes.CONTENT_URI, null, null,
							null, null);
			if (cursor != null && cursor.moveToFirst()) {

				do {
					entities.add(loadFromPopularRecipeCursor(cursor));
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return entities;
	}

    @Override
    public ArrayList<PopularRecipesEntity> getFavRecipes() {
        Cursor cursor = null;
        ArrayList<PopularRecipesEntity> entities = new ArrayList<PopularRecipesEntity>();
        try {

            cursor = AppController.getInstance()
                    .getContentResolver()
                    .query(DatastoreContract.PopularRecipes.CONTENT_URI, null, DatastoreContract.PopularRecipes.COLUMN_IS_FAV +" = 1",
                            null, null);
            if (cursor != null && cursor.moveToFirst()) {

                do {
                    entities.add(loadFromPopularRecipeCursor(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return entities;
    }
	
	@Override
	public PopularRecipesEntity getPopularRecipe(String columnName,
			String value) {
		Cursor cursor = null;
        PopularRecipesEntity entity = null;
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.PopularRecipes.CONTENT_URI, null, columnName + " = ?",
							new String[] { value }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				entity = loadFromPopularRecipeCursor(cursor);
			}
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return entity;
	}

	public static final PopularRecipesEntity loadFromPopularRecipeCursor(Cursor c) {
        PopularRecipesEntity popularRecipesEntity = new PopularRecipesEntity(c.getInt(0),
				c.getInt(1), c.getString(2), c.getString(3), c.getInt(4));
		return popularRecipesEntity;
	}

	public boolean keyAlreadyExists(Uri uri, String columnname, String key) {
		Cursor cursor = null;
		boolean exists = false;
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(uri, null, columnname + " = ?",
							new String[] { key }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				exists = true;
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return exists;
	}

	public PopularRecipesEntity keyAlreadyExists(String columnname, String key) {
		Cursor cursor = null;
        PopularRecipesEntity entity = null;
		try {

			cursor = AppController.getInstance()
					.getContentResolver()
					.query(DatastoreContract.PopularRecipes.CONTENT_URI, null, columnname + " = ?",
							new String[] { key }, null);
			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				entity = loadFromPopularRecipeCursor(cursor);
			}
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
		return entity;
	}

	@Override
	public void deleteAllPopularRecipes() {
		try {

            AppController.getInstance()
					.getContentResolver()
					.delete(DatastoreContract.PopularRecipes.CONTENT_URI, null, null);
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		}
	}

	@Override
	public void deletePopularRecipe(String columnName, String value) {
		try {

            AppController.getInstance()
					.getContentResolver()
					.delete(DatastoreContract.PopularRecipes.CONTENT_URI, columnName + " = ?",
							new String[] { value });
		} catch (Exception e) {
            Log.e(TAG, e.getMessage());
		}

	}


	@Override
	public void markAsFav(String columnName, String value) {
		final ContentValues values = new ContentValues();
		values.put(DatastoreContract.PopularRecipes.COLUMN_IS_FAV,
				1);
        AppController.getInstance()
				.getContentResolver()
				.update(DatastoreContract.PopularRecipes.CONTENT_URI, values, columnName + " = ?",
						new String[] { "" + value });

	}

    @Override
    public void markAsUnFav(String columnName, String value) {
        final ContentValues values = new ContentValues();
        values.put(DatastoreContract.PopularRecipes.COLUMN_IS_FAV,
                0);
        AppController.getInstance()
                .getContentResolver()
                .update(DatastoreContract.PopularRecipes.CONTENT_URI, values, columnName + " = ?",
                        new String[] { "" + value });

    }
}
