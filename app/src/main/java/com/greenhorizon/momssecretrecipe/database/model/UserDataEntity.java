package com.greenhorizon.momssecretrecipe.database.model;

public class UserDataEntity {
	private String mKey;
	private String mValue;
	
	public UserDataEntity(String mKey, String mValue){
		this.mKey = mKey;
		this.mValue = mValue;
	}
	public String getmKey() {
		return mKey;
	}
	public void setmKey(String mKey) {
		this.mKey = mKey;
	}
	public String getmValue() {
		return mValue;
	}
	public void setmValue(String mValue) {
		this.mValue = mValue;
	}
}
