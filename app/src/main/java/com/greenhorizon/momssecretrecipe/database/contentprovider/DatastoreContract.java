package com.greenhorizon.momssecretrecipe.database.contentprovider;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatastoreContract {
	public static final String AUTHORITY = "com.momsecretrecipe.provider";
	private static final String CONTENT_PREFIX = "content://";

	public static class UserData implements UserDataColumns {
		public static final String TABLE_NAME = "UserData";
		public static final Uri CONTENT_URI = Uri.parse(CONTENT_PREFIX + AUTHORITY + "/" + TABLE_NAME);
		
		public static final Uri contentUriFor(long id) {
			return ContentUris.withAppendedId(CONTENT_URI, id);
		}
	}
	
	protected static interface UserDataColumns extends BaseColumns {
		public static final String KEY = "key";
		public static final String VALUE = "value";
		 
	}
		
	public static class PopularRecipes implements PopularRecipeColumns {
		public static final String TABLE_NAME = "popularrecipe";
		public static final Uri CONTENT_URI = Uri.parse(CONTENT_PREFIX + AUTHORITY + "/" + TABLE_NAME);
		
		public static final Uri contentUriFor(long id) {
			return ContentUris.withAppendedId(CONTENT_URI, id);
		}
	}

	protected static interface PopularRecipeColumns extends BaseColumns {
		public static final String COLUMN_RECIPE_ID = "recipe_id";
		public static final String COLUMN_RECIPE_IMG_URL = "recipe_img_url";
		public static final String COLUMN_RECIPE_DATA = "recipe_data";
		public static final String COLUMN_IS_FAV = "is_fav";
	}
	
	public static class MyRecipes implements MyRecipeColumns {
		public static final String TABLE_NAME = "myrecipe";
		public static final Uri CONTENT_URI = Uri.parse(CONTENT_PREFIX + AUTHORITY + "/" + TABLE_NAME);
		
		public static final Uri contentUriFor(long id) {
			return ContentUris.withAppendedId(CONTENT_URI, id);
		}
	}

	protected static interface MyRecipeColumns extends BaseColumns {
        public static final String COLUMN_RECIPE_ID = "recipe_id";
        public static final String COLUMN_RECIPE_IMG_URL = "recipe_img_url";
        public static final String COLUMN_RECIPE_DATA = "recipe_data";
        public static final String COLUMN_IS_FAV = "is_fav";
	}
}
