package com.greenhorizon.momssecretrecipe.database;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.greenhorizon.momssecretrecipe.database.contentprovider.DbSchema;

/**
 * Implementation of the Android SQLite.
 */
public class SqliteDatabaseUtils implements SqliteDatabase {

    private SqliteOpenHelperExt mHelper;
    private static String TAG = "SqliteDatabaseUtils";
    public SqliteDatabaseUtils(Context context) {
    	mHelper = new SqliteOpenHelperExt(context, DbSchema.DB_NAME, null, DbSchema.SCHEMA_VERSION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate() {
        //Ln.i("onCreate");
        // No table pre-creations. The DAOs are responsible to create the
        // tables.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onUpgrade(int oldVersion, int newVersion) {
        //Ln.i("onUpgrade");
        // Nothing to do (for now).
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        try {
            return mHelper.getWritableDatabase();
        } catch (SQLException e) {
            throw new IllegalArgumentException("Unable to create dao", e);
        }
    }
    
    
    @Override
	public SQLiteDatabase getReadableDatabase() {
    	try {
            return mHelper.getReadableDatabase();
        } catch (SQLException e) {
            throw new IllegalArgumentException("Unable to read dao", e);
        }
	}

    /**
     * This subclass is the interface with the Android SQLite database; it has
     * callbacks for creation and database ugprade.
     * 
     */
    private class SqliteOpenHelperExt extends SQLiteOpenHelper {
    	
        SqliteOpenHelperExt(Context context, String databaseName, CursorFactory factory, int databaseVersion) {
            super(context, databaseName, factory, databaseVersion);
        }
        
        @Override
    	public void onOpen(SQLiteDatabase db) {
    		super.onOpen(db);
    		if (!db.isReadOnly()) {
    			// Enable foreign key constraints
    			db.execSQL("PRAGMA foreign_keys=ON;");
    		}
    	}
        
    	@Override
    	public void onCreate(SQLiteDatabase database) {
    
    		try {
    			database.beginTransaction();
    			for (int i = 1; i <= DbSchema.SCHEMA_VERSION; i++) {
    				switch (i) {
    
    				case 1:
    					if (database.isOpen()) {
    						database.execSQL(DbSchema.UserData.CREATE_DEFINITION);
    						database.execSQL(DbSchema.PopularRecipes.CREATE_DEFINITION);
    						database.execSQL(DbSchema.MyRecipes.CREATE_DEFINITION);
    					}
    					else {
    						Log.d(TAG, "Database not open ");
    					}
    					break;
    				
    				}
    			}
    			database.setTransactionSuccessful();
    		}
    		catch (SQLException se) {
                Log.e(TAG, ""+se);
    		}
    		catch (Exception e) {
                Log.e(TAG, ""+e);
    		}
    		finally {
    			database.endTransaction();
    		}
    	}
    
    	@Override
    	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {

            Log.e(TAG, "upgrading database "+oldVersion+" "+newVersion);
    		try {
    			database.beginTransaction();
    			for (int i = oldVersion + 1; i <= newVersion; i++) {
    				// Future schema changes has to go into this loop
    				switch (i) {
 
    				}
    			}
    			database.setTransactionSuccessful();
    		}
    		catch (Exception e) {
                Log.e(TAG, ""+e);
    		}
    		finally {
    			database.endTransaction();
    		}
    		
    	}
    }
}
