package com.greenhorizon.momssecretrecipe.database.contentprovider;

public class DbSchema {
    public static final String DB_NAME = "secretrecipe.db";

    public static final int SCHEMA_VERSION = 1;
    public static final int IS_FAV_DEFAULT = 0;

    public final static class UserData extends DatastoreContract.UserData {
        public static final String CREATE_DEFINITION = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
                +  KEY + " TEXT NOT NULL, "
                +  VALUE + " MEDIUMTEXT NULL"
                +  ", PRIMARY KEY ("+KEY+")"
                + ");";
    }

    public final static class PopularRecipes extends DatastoreContract.PopularRecipes {
        public static final String CREATE_DEFINITION = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("+ _ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                +  COLUMN_RECIPE_ID + " INT, "
                +  COLUMN_RECIPE_DATA + " TEXT NULL, "
                +  COLUMN_RECIPE_IMG_URL + " TEXT NULL,  "
                +  COLUMN_IS_FAV + " TINYINT(1) DEFAULT "+IS_FAV_DEFAULT
                + ");";
    }

    public final static class MyRecipes extends DatastoreContract.MyRecipes {
        public static final String CREATE_DEFINITION = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("+ _ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                +  COLUMN_RECIPE_ID + " INT, "
                +  COLUMN_RECIPE_DATA + " TEXT NULL, "
                +  COLUMN_RECIPE_IMG_URL + " TEXT NULL,  "
                +  COLUMN_IS_FAV + " TINYINT(1) DEFAULT "+IS_FAV_DEFAULT
                + ");";
    }
}
