package com.greenhorizon.momssecretrecipe;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.greenhorizon.momssecretrecipe.adapter.MyRecipeAdapter;
import com.greenhorizon.momssecretrecipe.adapter.ReviewAdapter;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Reviews;

import java.util.ArrayList;


public class MyRecipeActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_recipe);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        ListView listview = (ListView) findViewById(R.id.myrecipe_list);
        final ArrayList<MyRecipesEntity> arrMyRecipes  = AppController.getInstance().getDaoService().getMyRecipesDao().getMyRecipes();
        if (arrMyRecipes != null && arrMyRecipes.size() > 0) {
            MyRecipeAdapter adapter = new MyRecipeAdapter(this, arrMyRecipes);
            listview.setAdapter(adapter);
        }

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                MyRecipesEntity entity = arrMyRecipes.get(position);
                Intent intent = null;
                intent = new Intent(MyRecipeActivity.this, RecipeActivity.class);
                intent.putExtra("id", ""+entity.getmId());
                intent.putExtra("myrecipe", true);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        // Handle action buttons
        switch (item.getItemId()) {
            case android.R.id.home:
                intent = new Intent(getApplicationContext(),
                        Dashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
