package com.greenhorizon.momssecretrecipe.services.impl;

import android.util.Log;

import com.greenhorizon.momssecretrecipe.database.IMyRecipesData;
import com.greenhorizon.momssecretrecipe.database.IPopularRecipesData;
import com.greenhorizon.momssecretrecipe.database.IUserData;
import com.greenhorizon.momssecretrecipe.database.impl.MyRecipeDataImpl;
import com.greenhorizon.momssecretrecipe.database.impl.PopularRecipeDataImpl;
import com.greenhorizon.momssecretrecipe.database.impl.UserDataImpl;
import com.greenhorizon.momssecretrecipe.services.IDaoService;

public class DaoServiceImpl implements IDaoService {

	private static String TAG = "DaoServiceImpl";

	private IUserData userDataDao;

	private IPopularRecipesData popularRecipesData;

	private IMyRecipesData myRecipesData;

	public DaoServiceImpl() {
		super();
		InitializeDB();
	}

	void InitializeDB() {
		try {
			userDataDao = new UserDataImpl();
            popularRecipesData = new PopularRecipeDataImpl();
            myRecipesData = new MyRecipeDataImpl();
		} catch (Exception ex) {
			Log.e(TAG, ""+ex);
		}
	}

	@Override
	public IUserData getUserDataDao() {
		return userDataDao;
	}

	@Override
	public IPopularRecipesData getPopularRecipesDao() {
		return popularRecipesData;
	}

	@Override
	public IMyRecipesData getMyRecipesDao() {
		return myRecipesData;
	}

	@Override
	public void deleteAllData() {
		try {
			userDataDao.deleteAllUserDetails();
            popularRecipesData.deleteAllPopularRecipes();
            myRecipesData.deleteAllMyRecipes();
		} catch (Exception ex) {
            Log.e(TAG, ""+ex);
		}
	}
}
