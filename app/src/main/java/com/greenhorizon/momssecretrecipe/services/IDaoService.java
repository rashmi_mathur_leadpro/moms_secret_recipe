package com.greenhorizon.momssecretrecipe.services;

import com.greenhorizon.momssecretrecipe.database.IMyRecipesData;
import com.greenhorizon.momssecretrecipe.database.IPopularRecipesData;
import com.greenhorizon.momssecretrecipe.database.IUserData;


public interface IDaoService {

	public IUserData getUserDataDao();
	
	public IPopularRecipesData getPopularRecipesDao();
	
	public IMyRecipesData getMyRecipesDao();

	public void deleteAllData();

}
