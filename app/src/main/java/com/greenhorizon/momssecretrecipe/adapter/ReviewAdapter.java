package com.greenhorizon.momssecretrecipe.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.greenhorizon.momssecretrecipe.R;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.PopularRecipe;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Reviews;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 06/03/15.
 */
public class ReviewAdapter extends ArrayAdapter<Reviews> {
    private Context mContext;
    private ArrayList<Reviews> reviews;

    public ReviewAdapter(Context mContext,
                         ArrayList<Reviews> reviews) {

        super(mContext, -1, reviews);
        this.mContext = mContext;
        this.reviews = reviews;
    }

    @Override
    public int getCount() {

        if (reviews != null)
            return reviews.size();

        return 0;
    }

    @Override
    public Reviews getItem(int position) {

        return reviews.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Reviews entity = reviews.get(position);
        if (convertView == null) {
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.review_row, null);
        } else {
            grid = (View) convertView;
        }


        TextView reviewBytextView = (TextView) grid.findViewById(R.id.reviewby);
        TextView commentstv = (TextView) grid.findViewById(R.id.reviewcomments);
        TextView reviewedOntv = (TextView) grid.findViewById(R.id.reviewedon);
        if(entity != null) {
            reviewBytextView.setText(entity.reviewername);
            commentstv.setText(entity.comment);
            reviewedOntv.setText(" "+entity.reviewedon);
            RatingBar ratingBar = (RatingBar)grid.findViewById(R.id.reciperating);
            ratingBar.setRating(entity.rating);
            LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        }

        final NetworkImageView nv = (NetworkImageView) grid.findViewById(R.id.contact_img);
        nv.setImageUrl(entity.reviewerpic, AppController.getInstance().getImageLoader());
        nv.setVisibility(View.INVISIBLE);

        AppController.getInstance().getImageLoader().get(entity.reviewerpic, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null) {
                    Bitmap bitmap = response.getBitmap();
                    if (bitmap != null) {
                        nv.setVisibility(View.VISIBLE);
                        // ** code to turn off the progress wheel **
                        // ** code to use the bitmap in your imageview **
                    }
                }
            }

            @Override
            public void onErrorResponse (VolleyError error){
                nv.setVisibility(View.VISIBLE);
                // ** code to handle errors **
            }

        });
        grid.setClickable(false);
        grid.setFocusable(false);
        return grid;
    }
}
