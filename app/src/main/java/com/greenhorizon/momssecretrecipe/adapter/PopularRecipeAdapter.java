package com.greenhorizon.momssecretrecipe.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.greenhorizon.momssecretrecipe.R;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.PopularRecipe;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 26/02/15.
 */
public class PopularRecipeAdapter extends ArrayAdapter<PopularRecipesEntity> {
    private Context mContext;
    private ArrayList<PopularRecipesEntity> popularRecipes;

    public PopularRecipeAdapter(Context mContext,
                           ArrayList<PopularRecipesEntity> arrPopularRecipes) {

        super(mContext, -1, arrPopularRecipes);
        this.mContext = mContext;
        this.popularRecipes = arrPopularRecipes;
    }

    @Override
    public int getCount() {

        if (popularRecipes != null)
            return popularRecipes.size();

        return 0;
    }

    @Override
    public PopularRecipesEntity getItem(int position) {

        return popularRecipes.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final PopularRecipesEntity entity = popularRecipes.get(position);
        if (convertView == null) {
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.popular_recipe_row, null);
        } else {
            grid = (View) convertView;
        }


        TextView textView = (TextView) grid.findViewById(R.id.grid_text);
        TextView profileNametv = (TextView) grid.findViewById(R.id.profilename);

        PopularRecipe recipe = AppController.getInstance().getGsonObj().fromJson(entity.getMrecipeData(), PopularRecipe.class);
        if(recipe != null) {
            textView.setText(recipe.recipename);
            profileNametv.setText("by "+recipe.profilename);
            RatingBar ratingBar = (RatingBar)grid.findViewById(R.id.rating);
            ratingBar.setRating(recipe.rating);
            LayerDrawable stars = (LayerDrawable) ratingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        }
//        imageView.setImageResource(mThumbIds[position]);

        final NetworkImageView nv = (NetworkImageView) grid.findViewById(R.id.photo);
        nv.setDefaultImageResId(R.drawable.recipe_pic); // image for loading...
        //reload
        nv.setImageUrl(entity.getMrecipeImgUrl(), AppController.getInstance().getImageLoader());
//        nv.setVisibility(View.INVISIBLE);

        AppController.getInstance().getImageLoader().get(entity.getMrecipeImgUrl(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if (response != null) {
                    Bitmap bitmap = response.getBitmap();
                    if (bitmap != null) {
//                        nv.setVisibility(View.VISIBLE);
                        // ** code to turn off the progress wheel **
                        // ** code to use the bitmap in your imageview **
                    }
                }
            }

            @Override
            public void onErrorResponse (VolleyError error){
//                nv.setVisibility(View.VISIBLE);
                // ** code to handle errors **
            }

        });
        grid.setClickable(false);
        grid.setFocusable(false);
        return grid;
    }
}
