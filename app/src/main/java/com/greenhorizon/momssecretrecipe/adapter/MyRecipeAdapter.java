package com.greenhorizon.momssecretrecipe.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.greenhorizon.momssecretrecipe.R;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.MyRecipe;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Reviews;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 06/03/15.
 */
public class MyRecipeAdapter extends ArrayAdapter<MyRecipesEntity> {
    private Context mContext;
    private ArrayList<MyRecipesEntity> myrecipes;

    public MyRecipeAdapter(Context mContext,
                           ArrayList<MyRecipesEntity> myrecipes) {

        super(mContext, -1, myrecipes);
        this.mContext = mContext;
        this.myrecipes = myrecipes;
    }

    @Override
    public int getCount() {

        if (myrecipes != null)
            return myrecipes.size();

        return 0;
    }

    @Override
    public MyRecipesEntity getItem(int position) {

        return myrecipes.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final MyRecipesEntity entity = myrecipes.get(position);
        if (convertView == null) {
            view = new View(mContext);
            view = inflater.inflate(R.layout.myrecipe_row, null);
        } else {
            view = (View) convertView;
        }


        TextView recipeNametextView = (TextView) view.findViewById(R.id.recipename);
        if(entity != null) {
            MyRecipe recipe = AppController.getInstance().getGsonObj().fromJson(entity.getMrecipeData(), MyRecipe.class);
            recipeNametextView.setText(recipe.recipename);
        }

        final ImageView imgRecipeSnap = (ImageView) view.findViewById(R.id.recipesnap);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        Bitmap bitmap = BitmapFactory.decodeFile(entity.getMrecipeImgUrl(),
                options);

        imgRecipeSnap.setImageBitmap(bitmap);

        view.setClickable(false);
        view.setFocusable(false);
        return view;
    }
}
