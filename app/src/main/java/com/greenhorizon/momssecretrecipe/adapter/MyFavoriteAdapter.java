package com.greenhorizon.momssecretrecipe.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.greenhorizon.momssecretrecipe.R;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.MyRecipe;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 06/03/15.
 */
public class MyFavoriteAdapter extends ArrayAdapter<PopularRecipesEntity> {
    private Context mContext;
    private ArrayList<PopularRecipesEntity> myfavrecipes;

    public MyFavoriteAdapter(Context mContext,
                             ArrayList<PopularRecipesEntity> myfavrecipes) {

        super(mContext, -1, myfavrecipes);
        this.mContext = mContext;
        this.myfavrecipes = myfavrecipes;
    }

    @Override
    public int getCount() {

        if (myfavrecipes != null)
            return myfavrecipes.size();

        return 0;
    }

    @Override
    public PopularRecipesEntity getItem(int position) {

        return myfavrecipes.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final PopularRecipesEntity entity = myfavrecipes.get(position);
        if (convertView == null) {
            view = new View(mContext);
            view = inflater.inflate(R.layout.myfav_row, null);
        } else {
            view = (View) convertView;
        }


        TextView recipeNametextView = (TextView) view.findViewById(R.id.recipename);
        if(entity != null) {
            MyRecipe recipe = AppController.getInstance().getGsonObj().fromJson(entity.getMrecipeData(), MyRecipe.class);
            recipeNametextView.setText(recipe.recipename);

            NetworkImageView profilepicnv = (NetworkImageView) view.findViewById(R.id.profilepic);
            profilepicnv.setImageUrl(recipe.profilepic, AppController.getInstance().getImageLoader());

        }

        final NetworkImageView nv = (NetworkImageView) view.findViewById(R.id.recipesnap);
        nv.setDefaultImageResId(R.drawable.recipe_pic); // image for loading...
        nv.setImageUrl(entity.getMrecipeImgUrl(), AppController.getInstance().getImageLoader());

        view.setClickable(false);
        view.setFocusable(false);
        return view;
    }
}
