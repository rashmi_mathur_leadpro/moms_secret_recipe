package com.greenhorizon.momssecretrecipe.fragments;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TimePicker;

import com.greenhorizon.momssecretrecipe.R;

public class MomSecretRecipeDialog extends DialogFragment {

	public static final int ALERT_DIALOG = 1;
	ProgressDialog progressDialog;
	AlertDialog dialog;
	public static final int PROGRESS_DIALOG = 2;
	public static final int ERROR_MESSAGE_DIALOG = 3;
	public static final int PROGRESS_PERCENTAGE_DIALOG = 9;
	public static final int CONFIRMATION_DIALOG = 10;
	public static final int RADIOBUTTON_DIALOG = 15;
	int hour = 0;
	int minute = 0;
	Context context;

	public interface MomSecretDialogCompliant {
		public void doOkClick(String tag);

		public void doOkErrorDailogClick(String tag);

		public void doOkConfirmClick(String tag);

		public void doCancelConfirmClick(String tag);

        public void onRadioSelection(int status);

        public void onRadioSelected();
	}

	private MomSecretDialogCompliant caller;

	public MomSecretRecipeDialog(MomSecretDialogCompliant caller, int title,
                                 String message, int type, String tag) {
		Bundle args = new Bundle();
		args.putInt("title", title);
		args.putString("msg", message);
		args.putInt("type", type);
		args.putString("tag", tag);
		this.caller = caller;
		setArguments(args);
	}

	public MomSecretRecipeDialog(MomSecretDialogCompliant caller, int title,
                                 String message, int type, boolean cancelable) {
		Bundle args = new Bundle();
		args.putInt("title", title);
		args.putString("msg", message);
		args.putInt("type", type);
		args.putBoolean("cancelable", cancelable);
		this.caller = caller;
		setArguments(args);
	}

	public MomSecretRecipeDialog() {

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		int title = getArguments().getInt("title");
		int type = getArguments().getInt("type");
		String message = getArguments().getString("msg");
		boolean cancelable = getArguments().getBoolean("cancelable");
		final String tag = getArguments().getString("tag");
		Dialog dialog;
		Builder builder = null;
		LayoutInflater inflater = null;
		LinearLayout ll = null;
		switch (type) {
		case ALERT_DIALOG:
			builder = new Builder(getActivity())
					.setTitle(title)
					.setMessage(message)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									caller.doOkClick(tag);
								}
							});
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			return dialog;
		case PROGRESS_DIALOG:
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setIndeterminate(true);
			progressDialog.setMessage(message);
			progressDialog.setCancelable(cancelable);
			if (message != null) {
				progressDialog.setMessage(message);
			}
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.setCancelable(false);
			return progressDialog;

		case PROGRESS_PERCENTAGE_DIALOG:
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			progressDialog.setMax(100);
			progressDialog.setIndeterminate(false);
			if (message != null) {
				progressDialog.setMessage(message);
			}
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.setCancelable(false);
			return progressDialog;
		case ERROR_MESSAGE_DIALOG:
			builder = new Builder(getActivity());
			builder.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							caller.doOkErrorDailogClick(tag);
						}
					});
			if (message != null) {
				builder.setMessage(message);
			}
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			return dialog;
		case CONFIRMATION_DIALOG:
			builder = new Builder(getActivity());
			builder.setPositiveButton(getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							caller.doOkConfirmClick(tag);
						}
					});
			builder.setNegativeButton(getString(R.string.cancel),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							caller.doCancelConfirmClick(tag);
						}
					});
			if (message != null) {
				builder.setMessage(message);
			}
			dialog = builder.create();
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			setCancelable(false);
			return dialog;
            case RADIOBUTTON_DIALOG:
                builder = new AlertDialog.Builder(getActivity());
                // Set the dialog title
                builder.setTitle(R.string.pref_title_preferred_language)
                        // Specify the list array, the items to be selected by
                        // default (null for none),
                        // and the listener through which to receive callbacks when
                        // items are selected
                        .setSingleChoiceItems(R.array.pref_language_titles, -1,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        caller.onRadioSelection(which);

                                    }
                                });
                                // Set the action buttons

                dialog = builder.create();
                dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(android.content.DialogInterface dialog, int keyCode, android.view.KeyEvent event) {

                        if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                            //Hide your keyboard here!!!
                            return true; // pretend we've processed it
                        } else
                            return false; // pass on to be processed as normal
                    }
                });
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                return dialog;
		default:
			break;
		}

		return null;
	}

	private static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}
}
