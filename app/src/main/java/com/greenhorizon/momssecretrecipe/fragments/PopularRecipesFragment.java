package com.greenhorizon.momssecretrecipe.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.greenhorizon.momssecretrecipe.Dashboard;
import com.greenhorizon.momssecretrecipe.R;
import com.greenhorizon.momssecretrecipe.RecipeActivity;
import com.greenhorizon.momssecretrecipe.adapter.PopularRecipeAdapter;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.contentprovider.DatastoreContract;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.PopularRecipe;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 06/03/15.
 */
public class PopularRecipesFragment extends Fragment {
    public static final String ARG_SCREEN_NUMBER = "screen_number";
    private GridView gridview;
    PopularRecipeAdapter adapter;

    public PopularRecipesFragment() {
        // Empty constructor required for fragment subclasses

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = null;
        rootView = inflater.inflate(R.layout.popular_recipe, container, false);

        gridview = (GridView) rootView.findViewById(R.id.popular_recipe_grid);
        final ArrayList<PopularRecipesEntity> arrPopularRecipes = AppController.getInstance().getDaoService().getPopularRecipesDao().getPopularRecipes();
        if(arrPopularRecipes != null && arrPopularRecipes.size() >  0) {
            adapter = new PopularRecipeAdapter(getActivity()
                    .getApplicationContext(), arrPopularRecipes);
            gridview.setAdapter(adapter);
        }


        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                PopularRecipesEntity entity = arrPopularRecipes.get(position);
                Intent intent = null;
                intent = new Intent(((Dashboard) getActivity()),
                        RecipeActivity.class);
                intent.putExtra("id", ""+entity.getmId());
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);

            }

        });
        getActivity().setTitle("Dashboard");
        updateListDetails();
        return rootView;
    }

    public void updateListDetails() {
        gridview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
