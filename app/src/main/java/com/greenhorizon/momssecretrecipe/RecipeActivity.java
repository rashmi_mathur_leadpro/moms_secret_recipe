package com.greenhorizon.momssecretrecipe;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TableRow.LayoutParams;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.plus.PlusShare;
import com.greenhorizon.momssecretrecipe.adapter.PopularRecipeAdapter;
import com.greenhorizon.momssecretrecipe.adapter.ReviewAdapter;
import com.greenhorizon.momssecretrecipe.control.AppController;
import com.greenhorizon.momssecretrecipe.database.contentprovider.DatastoreContract;
import com.greenhorizon.momssecretrecipe.database.model.MyRecipesEntity;
import com.greenhorizon.momssecretrecipe.database.model.PopularRecipesEntity;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Directions;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Ingredients;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.MyRecipe;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.PopularRecipe;
import com.greenhorizon.momssecretrecipe.network.jsonmodel.Reviews;
import com.greenhorizon.momssecretrecipe.util.LoadProfileImage;
import com.greenhorizon.momssecretrecipe.util.NotifyingScrollView;
import com.greenhorizon.momssecretrecipe.util.StringUtils;

import java.util.ArrayList;

/**
 * Created by rashmimathur on 07/03/15.
 */
public class RecipeActivity extends ActionBarActivity {

    private static String TAG = "RecipeActivity";
    private Drawable mActionBarBackgroundDrawable;
    private ActionBar actionBar;
    private LinearLayout textContentHolder = null;
    private ImageButton btnFav;
    private PopularRecipesEntity entity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.recipe_detail);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        btnFav = (ImageButton)findViewById(R.id.btn_fav);

        mActionBarBackgroundDrawable = new ColorDrawable(Color.parseColor("#3E3A36"));
        mActionBarBackgroundDrawable.setAlpha(0);
        actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(mActionBarBackgroundDrawable);

        textContentHolder = (LinearLayout)findViewById(R.id.text_content_holder);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mActionBarBackgroundDrawable.setCallback(mDrawableCallback);
        }

        ((NotifyingScrollView) findViewById(R.id.scroll_view)).setOnScrollChangedListener(mOnScrollChangedListener);

        TableLayout tableLayout = (TableLayout) findViewById(R.id.tab_ingredients);
        TableLayout tableLayoutInstructions = (TableLayout) findViewById(R.id.tab_instructions);

        if(getIntent().getBooleanExtra("myrecipe", false)){
            ((Button) findViewById(R.id.btn_writereview)).setVisibility(View.GONE);
            MyRecipesEntity entity = AppController.getInstance().getDaoService().getMyRecipesDao().getMyRecipe(DatastoreContract.MyRecipes._ID, getIntent().getStringExtra("id"));
            if(entity != null) {
                MyRecipe recipe = AppController.getInstance().getGsonObj().fromJson(entity.getMrecipeData(), MyRecipe.class);
                if (recipe != null) {
                    ArrayList<Ingredients> arrIngredients = recipe.ingredients;
                    if (arrIngredients != null && arrIngredients.size() > 0) {
                        displayIngredients(arrIngredients, tableLayout);

                    }

                    ArrayList<Directions> arrDirections = recipe.directions;
                    if (arrDirections != null && arrDirections.size() > 0) {
                        displayDirections(arrDirections, tableLayoutInstructions);

                    }
                    ((TextView) findViewById(R.id.txt_recipename)).setText(recipe.recipename);
                    NetworkImageView profilepicnv = (NetworkImageView) findViewById(R.id.profilepic);
                    String personPhotoUrl = LoadProfileImage.getProfilePicUrl(this.getApplicationContext());
                    if(StringUtils.IsStringNotEmpty(personPhotoUrl)) {
                        personPhotoUrl = personPhotoUrl.substring(0,
                                personPhotoUrl.length() - 2)
                                + 100;
                    }
                    //reload
                    profilepicnv.setImageUrl(personPhotoUrl, AppController.getInstance().getImageLoader());

                    ((TextView) findViewById(R.id.recipeby)).setText("Recipe by " + LoadProfileImage.getProfileName(this.getApplicationContext()));

                    ListView listview = (ListView) findViewById(R.id.reviews_list);
                    ArrayList<Reviews> arrReviews = recipe.reviews;
                    if (arrReviews != null && arrReviews.size() > 0) {
                        ReviewAdapter adapter = new ReviewAdapter(this, arrReviews);
                        listview.setAdapter(adapter);
                        updateListViewHeight(listview);
                    }
                }
                final NetworkImageView nv = (NetworkImageView) findViewById(R.id.image_header);
                //reload
                nv.setImageUrl(entity.getMrecipeImgUrl(), AppController.getInstance().getImageLoader());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 4;
                Bitmap bitmap = BitmapFactory.decodeFile(entity.getMrecipeImgUrl(),
                        options);

                nv.setImageBitmap(bitmap);
            }
        }else{
            entity = AppController.getInstance().getDaoService().getPopularRecipesDao().getPopularRecipe(DatastoreContract.PopularRecipes._ID, getIntent().getStringExtra("id"));
            if(entity != null) {
                final Drawable originalIcon = getApplicationContext().getResources().getDrawable(R.drawable.menu_icon_bookmark);
                final Drawable icon = entity.getIsFav() == 1 ? originalIcon :convertDrawableToGrayScale(originalIcon);
                btnFav.setImageDrawable(icon);

                btnFav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        entity = AppController.getInstance().getDaoService().getPopularRecipesDao().getPopularRecipe(DatastoreContract.PopularRecipes._ID, getIntent().getStringExtra("id"));

                        int isFav = entity.getIsFav() == 1 ? 0:1;
                        if(isFav == 1) {
                            AppController.getInstance().getDaoService().getPopularRecipesDao().markAsFav(DatastoreContract.PopularRecipes._ID, "" + entity.getmId());
                            btnFav.setImageResource(R.drawable.menu_icon_bookmark);
                        }
                        else {
                            AppController.getInstance().getDaoService().getPopularRecipesDao().markAsUnFav(DatastoreContract.PopularRecipes._ID, "" + entity.getmId());
                            btnFav.setImageResource(R.drawable.menu_icon_bookmark_grey);
                        }
                    }
                });

                PopularRecipe recipe = AppController.getInstance().getGsonObj().fromJson(entity.getMrecipeData(), PopularRecipe.class);
                if (recipe != null) {
                    ArrayList<Ingredients> arrIngredients = recipe.ingredients;
                    if (arrIngredients != null && arrIngredients.size() > 0) {
                        displayIngredients(arrIngredients, tableLayout);

                    }

                    ArrayList<Directions> arrDirections = recipe.directions;
                    if (arrDirections != null && arrDirections.size() > 0) {
                        displayDirections(arrDirections, tableLayoutInstructions);

                    }
                    ((TextView) findViewById(R.id.txt_recipename)).setText(recipe.recipename);
                    NetworkImageView profilepicnv = (NetworkImageView) findViewById(R.id.profilepic);
                    profilepicnv.setImageUrl(recipe.profilepic, AppController.getInstance().getImageLoader());

                    ((TextView) findViewById(R.id.recipeby)).setText(getResources().getString(R.string.recipe_by)+" " + recipe.profilename);


                    ListView listview = (ListView) findViewById(R.id.reviews_list);
                    ArrayList<Reviews> arrReviews = recipe.reviews;
                    if (arrReviews != null && arrReviews.size() > 0) {
                        ReviewAdapter adapter = new ReviewAdapter(this, arrReviews);
                        listview.setAdapter(adapter);
                        updateListViewHeight(listview);
                    }
                }
                final NetworkImageView nv = (NetworkImageView) findViewById(R.id.image_header);
                nv.setImageUrl(entity.getMrecipeImgUrl(), AppController.getInstance().getImageLoader());
//            nv.setVisibility(View.INVISIBLE);

                AppController.getInstance().getImageLoader().get(entity.getMrecipeImgUrl(), new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        if (response != null) {
                            Bitmap bitmap = response.getBitmap();
                            if (bitmap != null) {
//                            nv.setVisibility(View.VISIBLE);

                                // ** code to turn off the progress wheel **
                                // ** code to use the bitmap in your imageview **
                            }
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
//                    nv.setVisibility(View.VISIBLE);
                        // ** code to handle errors **
                    }

                });
            }


        }
    }

    public void updateListViewHeight(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            return;
        }
        // get listview height
        int totalHeight = 0;
        int adapterCount = myListAdapter.getCount();
        for (int size = 0; size < adapterCount; size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        totalHeight += 30;
        // Change Height of ListView
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight
                + (myListView.getDividerHeight() * (adapterCount - 1));
        myListView.setLayoutParams(params);
    }

    private void displayIngredients(ArrayList<Ingredients> arrIngredients,
                                    TableLayout layout) {
        TableRow trData;
        for (int pos = 0; pos < arrIngredients.size(); pos++) {
            trData = new TableRow(this);
            if(pos%2 == 0)
                trData.setBackgroundResource(R.drawable.roundshape);
            else
                trData.setBackgroundResource(R.drawable.roundshapeodd);
//            trData.setBackgroundColor(getResources().getColor(R.color.light_grey));
            TextView txtView = null;
            for (int cnt = 0; cnt < 1; cnt++) {
                txtView = new TextView(this);


                if (cnt == 0) {
                    Ingredients ingredients = arrIngredients.get(pos);
                    txtView.setText(ingredients.ingredient);
                }

                txtView.setTextSize(18);
                LayoutParams params = new LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.setMargins(15, 15, 15, 15);
                txtView.setLayoutParams(params);
                trData.addView(txtView);
            }
            layout.addView(trData);
            TableRow tr = new TableRow(this);
            tr.setBackgroundColor(getResources().getColor(R.color.border_grey));
            tr.setPadding(0, 0, 0, 2); //Border between rows
            layout.addView(tr);

        }

    }

    private void displayDirections(ArrayList<Directions> arrDirections,
                                   TableLayout layout) {
        TableRow trData;
        for (int pos = 0; pos < arrDirections.size(); pos++) {
            trData = new TableRow(this);
            if(pos%2 == 0)
                trData.setBackgroundResource(R.drawable.roundshape);
            else
                trData.setBackgroundResource(R.drawable.roundshapeodd);
//            trData.setBackgroundColor(getResources().getColor(R.color.light_grey));
            TextView txtView = null;
            for (int cnt = 0; cnt < 1; cnt++) {
                txtView = new TextView(this);


                if (cnt == 0) {
                    Directions directions = arrDirections.get(pos);
                    txtView.setText(directions.direction);
                }

                txtView.setTextSize(18);
                LayoutParams params = new LayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                params.setMargins(15, 15, 15, 15);
                txtView.setLayoutParams(params);
                trData.addView(txtView);
            }
            layout.addView(trData);
            TableRow tr = new TableRow(this);
            tr.setBackgroundColor(getResources().getColor(R.color.border_grey));
            tr.setPadding(0, 0, 0, 2); //Border between rows
            layout.addView(tr);

        }

    }

    private NotifyingScrollView.OnScrollChangedListener mOnScrollChangedListener = new NotifyingScrollView.OnScrollChangedListener() {
        public void onScrollChanged(ScrollView who, int l, int t, int oldl, int oldt) {
            final int headerHeight = findViewById(R.id.image_header).getHeight() - actionBar.getHeight();
            final float ratio = (float) Math.min(Math.max(t, 0), headerHeight) / headerHeight;
            final int newAlpha = (int) (ratio * 255);
            mActionBarBackgroundDrawable.setAlpha(newAlpha);

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)textContentHolder.getLayoutParams();

            Log.i(TAG, "params.topMargin = "+params.topMargin + "  t = "+t);
            int moveBy = Math.min(Math.max(t/20, 0), headerHeight);

            params.setMargins(params.leftMargin, params.topMargin - (moveBy), params.rightMargin, params.bottomMargin);

//            textContentHolder.setLayoutParams(params);

        }
    };

    private Drawable.Callback mDrawableCallback = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(Drawable who) {
            actionBar.setBackgroundDrawable(who);
        }

        @Override
        public void scheduleDrawable(Drawable who, Runnable what, long when) {
        }

        @Override
        public void unscheduleDrawable(Drawable who, Runnable what) {
        }
    };


    /**
     * Mutates and applies a filter that converts the given drawable to a Gray
     * image. This method may be used to simulate the color of disable icons in
     * Honeycomb's ActionBar.
     *
     * @return a mutated version of the given drawable with a color filter applied.
     */
    public static Drawable convertDrawableToGrayScale(Drawable drawable) {
        if (drawable == null)
            return null;

        Drawable res = drawable.mutate();
        res.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        return res;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_recipe_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case android.R.id.home:
                intent = new Intent(getApplicationContext(),
                        Dashboard.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_share_recipe:
                PlusShare.Builder builder = new PlusShare.Builder(this);
                builder.setType("text/plain");
                builder.setText("I am using a cool app Mom's Secret Recipe to create recipes. Download the app from http://bit.ly/momssecretrecipe");
                builder.setContentUrl(Uri.parse("http://www.greenhorizon.momssecretrecipe.com"));
                startActivityForResult(builder.getIntent(), 0);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}